### iran-react 

iran is a web gallery, to show the beauties of iran

#### Run

`npm install`

`npm run dev`

Visit `localhost:3000`

### Export

`npm run build`

`npm run export`

#### Demo

<a href="https://neoxus.ir/demo/iran-react">Demo</a>

##### iran-react is a part of http://neoxus.ir project

You are free to use as you like as long as you give the right credits to the project and to the designer


### <div>Designed by <a href="https://nikandlv.ir">Nikan Dalvand</a></div>
### <div>Photos from <a href="https://unsplash.com">Unsplash</a></div>
